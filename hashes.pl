#/usr/bin/perl
use strict;
use warnings;

# Either
# my %fruit_color = ("apple", "red", "banana", "yellow");

# Or
my %fruit_color = (
    "apple" => "red",
    "banana" => "yellow"
);

print $fruit_color{"apple"} . "\n";

my @fruits = keys %fruit_color;
my @colors = values %fruit_color;

my $nb_fruits = @fruits;

print "Nombre de fruits : $nb_fruits\n";
# Pour les clés
print "Nombre de fruits : @fruits\n";
# Pour les valeurs
print "Nombre de couleurs : @colors\n";