#/usr/bin/perl
use strict;
use warnings;

my $animal = "camel";
my $answer = 42;

print $animal;
print "The animal is $animal\n";
print "The square of $answer is ", $answer * $answer, "\n";

#prints contents of $_ by default
#print;
$_="totot";
#print;
if (defined($_))
{
    print "\$_=";
    print;
}
else
{
    print "\$_ is not defined\n";
}
undef $_;
print "\n";
if (defined($_))
{
    print;
}
else
{
    print "\$_ is not defined\n";
}