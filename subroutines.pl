#/usr/bin/perl
use strict;
use warnings;

sub logger
{
    my $logmessage = shift;
    open my $logfile, ">>", "my.log" or die "Could not open my.log : $!";
    print $logfile $logmessage;
}

logger "We have a logger subroutine!";

sub square
{
    my $num = shift;
    my $result = $num * $num;
    return $result;
}

my $sq = square(8);
print "$sq\n";