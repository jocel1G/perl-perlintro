#/usr/bin/perl
use strict;
use warnings;

#$_ = "foo";
if (/foo/)
{
    print "Contains foo";
}
else
{
    print "Does not contain foo";
}
print "\n";
my $a = "afoob";
if ($a =~ /foo/)
{
    print "\$a contains foo";
}
else
{
    print "\$a does not contain foo";
}
