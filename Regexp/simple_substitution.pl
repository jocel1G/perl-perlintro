#/usr/bin/perl
use strict;
use warnings;

$_ = "goo foo";
s/foo/bar/;

print "\$_=$_\n";

$a = "fff foo dld_foo";
# donnera plus bas $a=fff bar dld_bar
$a =~ s/foo/bar/g;
# donnera plus bas $a=fff bar dld_foo
#$a =~ s/foo/bar/;

print "\$a=$a\n";