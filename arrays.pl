#/usr/bin/perl
use strict;
use warnings;

# Either
#my @animals = ("camel", "lama", "owl");
# Or
#my @animals = qw(camel lama owl);
# Or
my @animals = qw/camel lama owl/;
my @numbers = (23, 42, 45.63);
my @mixed = ("camel", 42, 1.23);

print $animals[2] . "\n";
print $numbers[2] . "\n";

print $#animals . "\n";

print $mixed[$#mixed] . "\n";

# nombre d'éléments d'un tableau
print @animals . "\n";

# array slice
my @anim = @animals[0,1];
for my $an(@anim)
{
    print $an."\n";
}

#my anim2 = @animals[1..$#animals];
my @anim2 = @animals[1..$#animals];
#print $anim2[$#anim2]."\n";
# affiche lamaowl
# print @anim2;
# affiche lama owl
print "@anim2\n";
# affiche 2 ; scalar context
#print @anim2+0 . "\n";
print scalar @anim2 . "\n";