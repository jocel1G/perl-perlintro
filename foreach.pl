#/usr/bin/perl
use strict;
use warnings;

# Either
my %fruit_color = ("apple", "red", "banana", "yellow");
# Or

#my %fruit_color = (
#    "apple" => "red",
#    "banana" => "yellow"
#);

my @fruits = keys %fruit_color;
my @colors = values %fruit_color;

foreach (@fruits)
{
    print "This is element $_\n";
}

my $max = 1;

print $colors[$_] . "\n" foreach 0..$max;

# foreach my $key (@fruits)
# or
foreach my $key (keys %fruit_color)
{
    print "The value of $key is $fruit_color{$key}\n";
}